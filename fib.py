"""Fibonacci sequence."""

__author__ = "Julien Palard"
__version__ = "0.1"

import argparse
from functools import cache


@cache
def fib(n):
    """Fibonacci sequence."""
    if n < 2:
        return 1
    return fib(n - 1) + fib(n - 2)


def parse_args():
    """Parses command line arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument("n", type=int)
    parser.add_argument("--verbose", "-v", action="store_true", help="Be more verbose")
    return parser.parse_args()


def main():
    """Module entry point."""
    args = parse_args()
    if args.verbose:
        print(f"fib({args.n}) = {fib(args.n)}")
    else:
        print(fib(args.n))


if __name__ == "__main__":
    main()
